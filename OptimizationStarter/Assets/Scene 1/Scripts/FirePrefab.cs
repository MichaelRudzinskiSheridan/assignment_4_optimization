﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Camera ) )]
public class FirePrefab : MonoBehaviour
{
    //Prefab that will be fired
    public GameObject Prefab;
    //Speed it will be fired at
    public float FireSpeed = 5;

    // Update is called once per frame
    void Update()
    {
        //If player presses fire button
        if ( Input.GetButton( "Fire1" ) )
        {
            //Finds the direction to fire and instantiates the prefab in that direction
            Vector3 clickPoint = GetComponent<Camera>().ScreenToWorldPoint( Input.mousePosition + Vector3.forward );
            Vector3 FireDirection = clickPoint - this.transform.position;
            FireDirection.Normalize();
            GameObject prefabInstance = GameObject.Instantiate( Prefab, this.transform.position, Quaternion.identity, null );
            prefabInstance.GetComponent<Rigidbody>().velocity = FireDirection * FireSpeed;
        }
    }

}
