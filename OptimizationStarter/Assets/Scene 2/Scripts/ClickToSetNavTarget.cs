﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTarget : MonoBehaviour
{
    private void Start()
    {
        QualitySettings.vSyncCount = 0;
    }

    // Update is called once per frame
    void Update ()
    {
       
        //Find the main camera
        Camera camera = GameObject.Find( "Main Camera" ).GetComponent<Camera>();
        //Fire a raycast looking for the ground
        RaycastHit hit = new RaycastHit();
        if ( Physics.Raycast( camera.ScreenPointToRay( Input.mousePosition ), out hit, float.MaxValue, LayerMask.GetMask( "Ground" ) ) )
        {
            GetComponent<NavMeshAgent>().destination = hit.point;
        }
	}
}
