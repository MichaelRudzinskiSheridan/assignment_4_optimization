﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public float Radius;

    public void Update()
    {
        //If you collide with the player and the collectible gameobject, destroy the collectible
        Collider[] collidingColliders = Physics.OverlapSphere( this.transform.position, Radius );
        for ( int colliderIndex = 0; colliderIndex < collidingColliders.Length; ++colliderIndex )
        {
            if ( collidingColliders[colliderIndex].tag == "Player" )
            {
                Destroy( this.gameObject );
            }
        }
    }

}
